#!/usr/bin/python3

import json
import requests
import time

url = "https://api.bitbucket.org/2.0/repositories?fields=values.name,*.uuid,next&pagelen=100"
headers = {'Content-Type': 'application/json'}
x = 0
start = time.time()

f = open('repos_bitty.txt', 'w')
f.close()

try:
	while True:
		f = open('repos_bitty.txt','a+')

		r = requests.get(url, headers=headers)

		for names in r.json()['values']:
			print(names['name'],end=',')
			f.write(names['uuid']+'\n')
			x+=1

		f.close()

		url = r.json()['next']
		print()

		if(x%10000==0):
			print(str(x)+" repository extracted!!")
			break

except KeyError:
	print('All Repos Done')

end = time.time()

print(str(end-start)+' seconds taken for collecting and saving '+str(x)+" repository from bitbucket.org")